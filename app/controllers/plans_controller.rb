class PlansController < ApplicationController
  before_action :set_plan, only: [:show, :edit, :update, :destroy, :generate, :reset, :send_email]

  respond_to :html

  def index
    @plans = Plan.all
    respond_with(@plans)
  end

  def show
    @resources = Resource.all
    respond_with(@plan)
  end

  def new
    @plan = Plan.new(start_date: date_at_next_week_monday)
    respond_with(@plan)
  end

  def edit
  end

  def create
    @plan = Plan.new(plan_params)
    @plan.save
    respond_with(@plan)
  end

  def generate
    @plan.generate!
    respond_with @plan, location: plan_url(@plan)
  end

  def reset
    @plan.reset!
    respond_with @plan, location: plan_url(@plan)
  end

  def update
    @plan.update(plan_params)
    respond_with(@plan)
  end

  def destroy
    @plan.destroy
    respond_with(@plan)
  end

  def send_email
    @plan_email = PlanEmail.new plan_email_params
    @message = !request.get? && @plan_email.send_email
    if @message
      respond_with @plan_email, notice: "Email has been sent.", status: :created
    end
  end

  private

  def date_at_next_week_monday
    Date.today.at_beginning_of_week+7.days
  end

  def set_plan
    @plan = Plan.find(params[:id])
  end

  def plan_email_params
    !request.get? ? params.require(:plan_email).permit(:email).merge(plan: @plan) : {}
  end

  def plan_params
    params.require(:plan).permit(:start_date, :title)
  end
end
