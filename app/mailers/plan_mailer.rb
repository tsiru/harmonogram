class PlanMailer < ActionMailer::Base
  def plan(plan, email)
    @plan      = plan
    @resources = plan.assigments.map(&:resource)
    mail(to: email, subject: "plan #{plan.title}")
  end
end
