class PlanEmail
  include ActiveModel::Model

  attr_accessor :email, :plan

  validates :email, :plan, presence: true

  def send_email
    valid? && mailer.plan(plan, email).deliver
  end

  private

  def mailer
    PlanMailer
  end
end
