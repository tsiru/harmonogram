class Assigment < ActiveRecord::Base
  belongs_to :resource
  belongs_to :plan

  delegate :date, to: :shift

  validates :plan_id, :resource_id, presence: true

  def shift_at(day_offset)
    Shift.find(pattern[day_offset]) if pattern && pattern[day_offset]
  end

  def shift_at_set(day_offset, value)
    pattern[day_offset] = Shift.find(value).to_s
  end
end
