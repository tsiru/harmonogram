class Shift
  DAY    = 'day'.freeze
  EARLY  = 'early'.freeze
  LATE   = 'late'.freeze
  NIGHT  = 'night'.freeze
  ANYDAY = 'anyday'.freeze
  REST   = 'rest'.freeze

  REAL_TYPES    = [DAY, EARLY, LATE, NIGHT].freeze
  VIRTUAL_TYPES = [ANYDAY, REST].freeze
  DAY_TYPES     = [DAY, EARLY, LATE, ANYDAY].freeze
  TYPES         = (REAL_TYPES + VIRTUAL_TYPES).freeze

  attr_reader :type

  def initialize(type)
    @type = TYPES.find { |t| t && self.class.send(:key, t) == self.class.send(:key, type) }
    # raise "unknown shift type #{type.inspect}" unless @type
  end

  class << self
    def find(type)
      all[key(type)] ||= new(type).freeze
    end

    private

    def key(type)
      type.to_s.downcase[0]
    end

    def all
      @all ||= {}
    end
  end

  TYPES.each do |type|
    define_method :"#{type}?" do
      self.type == type
    end
  end

  def free?
    rest?
  end

  def work?
    day? || early? || late? || night? || anyday?
  end

  def eql?(shift)
    type == shift.type
  end

  def to_s
    type.to_s.upcase[0]
  end
end
