class Resource < ActiveRecord::Base
  validates :first_name, :last_name, :hours_per_week, presence: true
  validates :hours_per_week, numericality: true

  has_many :assigments

  serialize :allowed_shifts_type, Array

  before_save :make_allowed_shifts_type_uniq!

  def same_profile?(resource)
    resource.is_a?(self.class) && resource.profile_hash == profile_hash
  end

  def max_shifts(weeks: 1)
    ((4+hours_per_week*weeks)/8.0).floor
  end

  def full_name
    [*first_name, *last_name].join(' ')
  end

  def profile_hash
    [*allowed_shifts_type, hours_per_week].hash
  end

  def allowed_shifts_types_short
    allowed_shifts_type.map{ |type| type[0] }.join.upcase
  end

  def allowed_nights?
    allowed_shifts_type.any? { |type| type == Shift::NIGHT }
  end

  def allowed_anydays?
    allowed_shifts_type.any? { |type| Shift::DAY_TYPES.include? type }
  end

  private

  def make_allowed_shifts_type_uniq!
    allowed_shifts_type.uniq!
    allowed_shifts_type.reject(&:blank?)
  end
end
