class Plan < ActiveRecord::Base
  WEEKS_COUNT = 5

  validates :start_date, :title, presence: true
  validate :start_date_must_start_at_monday
  validate :start_date_must_be_unchanged, on: :update

  has_many :assigments

  before_validation :generate_title_if_empty!

  def weeks_count
    (dates.count / 7.0).ceil
  end

  def send_email?
    assigments.any?
  end

  def reset!
    assigments.destroy_all
  end

  def reset?
    assigments.any?
  end

  def generate!(resources = Resource.all)
    reset!
    PlanGenerator.new(self, resources).generate!
  end

  def generate?
    true
  end

  def dates
    (start_date..end_date)
  end

  def end_date
    start_date + WEEKS_COUNT.weeks - 1.day if start_date
  end

  def shift_for_resources_at(resource, date)
    return nil unless assigment = assigments.find { |assigment| assigment.resource == resource }
    Shift.find(assigment.pattern[date - start_date])
  end

  private

  def generate_title_if_empty!
    self.title = "#{start_date} - #{end_date}" if title.blank?
  end

  def start_date_must_start_at_monday
    errors.add(:start_date, 'must start at Monday') unless start_date.monday?
  end

  def end_date_must_be_unchanged
    errors.add(:start_date, 'read only value') if start_date_changed?
  end

end
