PlanGenerator

class PlanGenerator
  class ShiftSelector

    def initialize(validator, resource)
      @validator = validator
      @resource  = resource
    end

    def free
      @free ||= Pattern.new(Array.new(WEEK_LENGTH) { Shift.find :rest })
    end

    def where(types: Shift::TYPES, matching: [])
      if matching.any?
        # puts '-' * 80
        puts matching.count
        # puts matching.map(&:join)
        puts matching.transpose.map { |day| day.count(&:night?) }.join
        # puts '-' * 80
      end

      types = Array.wrap(types)
      if types.count == 1 && types.first == Shift::NIGHT
        return night_patterns.reject do |pattern|
          matching.any? { |matching_pattern| pattern.have_conflicting_work_day? matching_pattern }
        end
      else
        raise 'unimplemented'
      end
    end

    # def best
    #   @last = night_patterns.shuffle.max_by(&:score)
    # end

    # def sample
    #   @last = night_patterns.sample
    # end

    def night_patterns
      @night_patterns ||= base_night_patterns #.select(&:only_nights?)
    end

    delegate :count, to: :night_patterns

    private

    attr_accessor :resource, :validator

    NIGHT_SHIFTS = ['N', 'R'].map{ |type| Shift.find type }.freeze

    delegate :valid_for_week?, to: :validator

    def base_night_patterns
      @night_patterns ||= [].tap do |patterns|
        Variator.new(NIGHT_SHIFTS, WEEK_LENGTH).each do |variation|
          patterns << variation if valid_for_week? variation, resource
        end
      end
      @night_patterns.dup
    end

    def bests
      @bests ||= []
    end

    def used_samples
      @used_samples ||= []
    end
  end
end
