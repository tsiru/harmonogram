PlanGenerator

class PlanGenerator
  class WeekCoordinator
    def initialize(validator, resources)
      @validator = validator
      @resources = resources
    end

    def to_enum
      Enumerator.new do |set|
        loop do
          patterns = []

          begin
            resources.each_with_index do |resource, index|
              patterns[index] = selector_for_resource(resource).where(types: Shift::NIGHT, matching: patterns).sample || selector_for_resource(resource).free
            end
          end while not week_have_full_night_shift? patterns

          nights_plan = Hash[resources.zip patterns]
          set << nights_plan
        end
      end
    end

    delegate :each, :take, to: :to_enum

    private

    attr_accessor :validator, :resources

    def week_have_full_night_shift?(patterns)
      patterns.any? && patterns.transpose.all? { |day| day.count(&:night?) == 1 }
    end

    def shift_selectors
      @shift_selectors ||= {}
    end
  end
end
