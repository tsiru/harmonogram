PlanGenerator

class PlanGenerator
  class Pattern < Array
    def score
      work_days
    end

    def only_nights?
      all? { |shift| shift.night? or shift.rest? }
    end

    def nights
      count(&:night?)
    end

    def work_days
      count(&:work?)
    end

    def free_days
      count(&:free?)
    end

    def nights_in_row
      to_s.scan(/NN/).count
    end

    def to_s
      join
    end

    def inspect
      "<#{self.class.name} shifts:#{to_s.scan(/.{,7}/).reject(&:blank?).map{ |w| "[#{w}]"}.join(' ')}>"
    end

    def have_conflicting_work_day?(pattern)
      raise "expected #{self.class.name}, but #{pattern.class} given." unless pattern.is_a?(self.class)
      pattern.zip(self).any? { |shift1, shift2| shift1 && shift2 && shift1.work? && shift2.work? }
    end

    def or(pattern)
      copy = self.clone
      copy.or= pattern
      copy
    end

    def or=(pattern)
      check_type pattern
      each_with_index do |item, index|
        self[index] = item.free? ? pattern[index] : item
      end
      self
    end

    def intersects?(pattern)
      zip(pattern).any? { |a,b| !a.free? && !b.free? }
    end

    private

    def check_type(pattern)
      raise "expected #{self.class.name}, got #{pattern.class.name}" unless pattern.is_a? self.class
    end

  end
end
