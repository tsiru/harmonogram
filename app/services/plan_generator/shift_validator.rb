PlanGenerator

class PlanGenerator
  class ShiftValidator

    def valid?(variation, resource)
      return false if not have_at_least_one_free_day? variation
      return false if have_different_shifts_at_weekend? variation
      return false if exceed_allowed_shift_count? variation, resource
      return false if not valid_for_common? variation, resource
      return false if not have_at_least_two_free_weekends? variation
      return true
    end

    def valid_for_week?(variation, resource)
      return false if not have_at_least_one_free_day? variation
      return false if have_different_shifts_at_weekend? variation
      return false if exceed_allowed_shift_count? variation, resource
      return false if not valid_for_common? variation, resource
      return true
    end

    def valid_for_period?(variation, resource)
      return false if not valid_for_common? variation, resource
      return false if not have_at_least_two_free_weekends? variation
      return true
    end

    private

    def valid_for_common?(variation, resource)
      return false if have_more_nights_than_allowed? variation #M
      return false if not have_enoungh_rest_after_night? variation
      return false if have_unavailable_shift_type? variation, resource
      return true
    end

    def have_five_weeks?(variation)

    end

    def have_unavailable_shift_type?(variation, resource)
      allowed = resource.allowed_shifts_type.map{ |type| Shift.find type }
      if allowed.any?(&:day?) || allowed.any?(&:early?) || allowed.any?(&:late?)
        allowed << Shift.find(:anyday)
      end
      denied = variation.uniq.reject(&:free?) - allowed
      return denied.any?
    end

    def have_at_least_two_free_weekends?(variation)
      free_weekends = variation.to_s.scan(/.{7}/).count { |week| week =~ /[^N]RR$/ }
      weeks = variation.count / WEEK_LENGTH
      free_weekends >= weeks - 3
    end

    def exceed_allowed_shift_count?(variation, resource)
      variation.count(&:work?) > resource.max_shifts
    end

    def have_enoungh_rest_after_night?(variation)
      return false if variation.join =~ /N[^R^N]/ #M
      return false if variation.join =~ /N{2,}[^R^N]?[R^N][^R^N]/ #M
      return true
    end

    def have_more_nights_than_allowed?(variation)
      variation.nights > 3
    end

    def have_different_shifts_at_weekend?(variation)
      saturday, sunday = *variation.last(2)
      saturday != sunday && saturday.work? && sunday.work?
    end

    def have_at_least_one_free_day?(variation)
      variation.any? &:rest?
    end
  end
end

__END__

by design:
# For each day a nurse may start only one shift.

done:
# The number of consecutive night shifts is at most 3.
# The number of consecutive shifts (workdays) is at most 6.
# Following a series of at least 2 consecutive night shifts a 42 hours rest is required.
# The maximum number of night shifts is 3 per period of 5 consecutive weeks.

1 week for 1 resource:


to do:
# Cover needs to be fulfilled (i.e. no shifts must be left unassigned).
  Dokładnie tylu pracowników na zmianie ile trzeba

# A nurse must receive at least 2 weekends off duty per 5 week period. A weekend off duty lasts 60 hours including Saturday 00:00 to Monday 04:00.
  Przynajmniej 2 wolne weekendy w tygodniu

reimplement partially:
# Within a scheduling period a nurse is allowed to exceed the number of hours for which they are available for their department by at most 4 hours.
  Max 4 godziny nadgodzin w miesiącu


# During any period of 24 consecutive hours, at least 11 hours of rest is required.
  11 godzin przerwy między zmianami

# A night shift has to be followed by at least 14 hours rest. An exception is that once in a period of 21 days for 24 consecutive hours, the resting time may be reduced to 8 hours.
  po nocce przerwa albo koleja nocka, ale raz na 21 dni można po nocce dać późną zmianę

+-------+-------+-------+----+----+----+----+----+----+----+
| shift | start |   end | Mo | Tu | We | Th | Fr | St | Sd |
+-------+-------+-------+----+----+----+----+----+----+----+
| day   |  8:00 | 17:00 |  3 |  3 |  3 |  3 |  3 |  2 |  2 |
+-------+-------+-------+----+----+----+----+----+----+----+
| early |  7:00 | 16:00 |  3 |  3 |  3 |  3 |  3 |  2 |  2 |
+-------+-------+-------+----+----+----+----+----+----+----+
| late  | 14:00 | 23:00 |  3 |  3 |  3 |  3 |  3 |  2 |  2 |
+-------+-------+-------+----+----+----+----+----+----+----+
| night | 23:00 |  7:00 |  1 |  1 |  1 |  1 |  1 |  1 |  1 |
+-------+-------+-------+----+----+----+----+----+----+----+
