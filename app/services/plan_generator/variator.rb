PlanGenerator

class PlanGenerator
  class Variator
    def initialize(elements, length)
      @elements = elements
      @length   = length
    end

    delegate :each, :take, to: :to_enum

    private

    attr_reader :elements, :length

    def to_enum
      Enumerator.new do |set|
        variations(elements, length, set)
      end
    end

    def variations(elements, count, set, generated = Pattern.new)
      if count > 0
        elements.each do |element|
          variations(elements, count-1, set, generated.clone << element)
        end
      else
        set << generated.freeze
      end
    end
  end
end
