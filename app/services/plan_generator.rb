class PlanGenerator < Struct.new(:plan, :resources)
  WEEK_LENGTH = 7

  def generate!
    assign_nights!
    assing_any_days!
  end

  private

  def assing_any_days!
    master_offset = 0
    days.each_with_index do |day, offset|
      # if master_offset > offset
      #   next
      # end

      days_count = (day.saturday? || day.sunday?) ? 2 : 3
      shifts = [Shift::DAY, Shift::EARLY, Shift::LATE].map{ |type| days_count.times.map { Shift.find(type).to_s } }.flatten

      limit = 100

      while shifts.any?
        if limit == 0
          break
        #   master_offset = offset-2
        #   reset_day_assigments_till!(master_offset)
        #   throw "limit exceded"
        end
        limit -= 1

        # puts"day: #{day}, offset: #{offset}"
        sorted_resources.reject { |resource|
          # puts"resource: #{resource.full_name}"
          assigment = assigment_for_resource!(resource)
          assigment.shift_at(offset).night? || offset > 0 && assigment.shift_at(offset-1).night?
        }.shuffle.each { |resource|
          break unless shifts.any?
          assigment = assigment_for_resource!(resource)
          prev_pattern = assigment.pattern
          assigment.shift_at_set(offset, shifts.first)
          # if validator.valid? make_pattern(assigment.pattern), resource
            shifts.shift
          # else
            # assigment.pattern = prev_pattern
          # end
        }
      end
    end
    assigments.each(&:save!)
  # rescue
    # retry
  end

  def assign_nights!
    steps = [2, 3, 2]
    offset  = 0

    sorted_resources.each do |resource|
      pattern = days.map { 'r' }.join
      if resource.allowed_nights?
        first = steps.first
        (0...first).each { |i| pattern[i+offset] = 'n' }
        offset += first
        steps.rotate! 1
      end
      plan.assigments.create! resource: resource, pattern: pattern
    end
  end

  def reset_day_assigments_till!(offset)
    puts "begin of reset_day_assigments_till! "
    assigments.each do |assigment|
      pattern = make_pattern(assigment.pattern)
      (offset...(assigment.pattern.lenghth)).each do |offset|
        next if pattern[offset].nigth?
        pattern[offset] = Shift.find(Shift::REST)
      end
      puts "#{assigment.pattern} => #{pattern}"
      assigment.pattern = pattern
    end
  rescue => e
    puts e
  end

  def assigment_for_resource!(resource)
    @assigments_cache ||= {}
    @assigments_cache[resource] ||= assigments.find { |assigment| assigment.resource_id == resource.id } or raise "unable to find resource #{resource}"
  end

  def sorted_resources
    resources.shuffle
  end

  def make_pattern(string)
    PlanGenerator::Pattern.new string.chars.map { |char| Shift.find char }
  end

  # def generate!(drawNumber: 30)

  #   assigments = Hash[resources.map { |resource| [resource, Pattern.new(Array.new(plan.weeks_count) { selector_for_resource(resource).free }.sum)] }]

  #   begin
  #     resources.sample.tap do |resource|
  #       assigments.delete resource
  #       patterns = []

  #       plan.weeks_count.times do

  #         nights = selector_for_resource(resource).where(types: Shift::NIGHT, matching: assigments.values).shuffle

  #         valid_pattern = nil
  #         while nights.any?
  #           pattern = nights.shift
  #           if validator.valid_for_period? Pattern.new((patterns + [pattern]).sum), resource
  #             valid_pattern = pattern
  #             break
  #           end
  #         end
  #         valid_pattern ||= selector_for_resource(resource).free
  #         patterns << valid_pattern
  #       end
  #       assigments[resource] = Pattern.new patterns.sum
  #     end
  #   end while not all_nights_assigned? assigments.values

  #   assigments.each do |resource, pattern|
  #     plan.assigments.create! resource: resource, pattern: pattern.join
  #   end

  #   # // create necessary counters
  #   # List<ShiftCounter> dayShiftCounters = new ArrayList<ShiftCounter>();
  #   # List<ShiftCounter> nightShiftCounters = new ArrayList<ShiftCounter>();
  #   # ScheduleUtil.createWorkWeekCounters(dayShiftCounters, nightShiftCounters);
  #   # ScheduleUtil.createWeekendCounters(dayShiftCounters, nightShiftCounters);

  #   # List<NurseSto> nursesForFullJob = getNursesBasedOnWorkType(nurses, 36);
  #   # List<NurseSto> nursesFor20Hours = getNursesBasedOnWorkType(nurses, 20);
  #   # List<NurseSto> nursesFor32Hours = getNursesBasedOnWorkType(nurses, 32);
  #   # List<NurseSto> allNurses = new ArrayList<NurseSto>();
  #   # allNurses.addAll(nursesForFullJob);
  #   # allNurses.addAll(nursesFor32Hours);
  #   # allNurses.addAll(nursesFor20Hours);

  #   # drawNumber.times do
  #   #   # set avability for nights for all resources/nurses
  #   #   weeks.each do

  #   #   end
  #   # end

  #   # Map<Integer, List<NurseSto>> scheduleMap = new HashMap<Integer, List<NurseSto>>();
  #   # for (int k = 0; k < drawNumber; ++k) {
  #   #   ScheduleUtil.setNightAvailabilityForAllNurses(allNurses);
  #   #   for (int i = 0; i < ScheduleConfig.ALL_SCHEDULE_WEEK_NUMBER; ++i) {
  #   #     ScheduleUtil.setWeekAvailabilityForAllNurses(allNurses);
  #   #     if (i < ScheduleConfig.ALL_SCHEDULE_WEEK_NUMBER - 1) {
  #   #       if (i % 2 == 0) {
  #   #         ScheduleUtil.createWeekScheduleNights(feasiblePatternsWithNight40Hours, nursesForFullJob, i, dayShiftCounters, nightShiftCounters);
  #   #         ScheduleUtil.createWeekScheduleDays(feasiblePatternsWithoutNight40Hours, nursesForFullJob, i, dayShiftCounters, nightShiftCounters);
  #   #         ScheduleUtil.setProperPatternsForPartTimeWorker(feasiblePatterns32Hours, feasiblePatterns16Hours, nursesFor32Hours, nursesFor20Hours, i, dayShiftCounters, nightShiftCounters);
  #   #       } else {
  #   #         ScheduleUtil.createWeekScheduleNights(feasiblePatternsWithNight32Hours, nursesForFullJob, i, dayShiftCounters, nightShiftCounters);
  #   #         ScheduleUtil.createWeekScheduleDays(feasiblePatternsWithoutNight32Hours, nursesForFullJob, i, dayShiftCounters, nightShiftCounters);
  #   #         ScheduleUtil.setProperPatternsForPartTimeWorker(feasiblePatterns32Hours, feasiblePatterns24Hours, nursesFor32Hours, nursesFor20Hours, i, dayShiftCounters, nightShiftCounters);
  #   #       }
  #   #       ScheduleUtil.fillWeekScheduleUsingWrongPatterns(nursesFor32Hours, nursesFor20Hours, i, dayShiftCounters, nightShiftCounters);
  #   #     } else {
  #   #       List<NursePattern> partTimeNightPatterns = PatternUtil. (feasiblePatterns24Hours);
  #   #       partTimeNightPatterns.addAll(feasiblePatterns16Hours);
  #   #       ScheduleUtil.createWeekScheduleNights(partTimeNightPatterns, nursesFor20Hours, i, dayShiftCounters, nightShiftCounters);
  #   #       ScheduleUtil.createWeekScheduleDays(feasiblePatternsWithoutNight40Hours, nursesForFullJob, i, dayShiftCounters, nightShiftCounters);
  #   #       ScheduleUtil.fillWeekScheduleUsingWrongPatterns(nursesForFullJob, nursesFor32Hours, i, dayShiftCounters, nightShiftCounters);
  #   #     }
  #   #     ScheduleUtil.setEmptyPatternForNotNecessaryWorkers(allNurses, i);
  #   #   }
  #   #   scheduleMap.put(ScheduleUtil.wrongPatterns, allNurses);
  #   #   ScheduleUtil.wrongPatterns = 0;
  #   # }

  #   # SortedSet<Integer> keys = new TreeSet<Integer>(scheduleMap.keySet());
  #   # for (Integer key : keys) {
  #   #   allNurses = scheduleMap.get(key);
  #   #   System.out.println("Wrong patterns number: " + key);
  #   #   break;
  #   # }

  #   # for (NurseSto nurse : allNurses) {
  #   #   for (int i = 0; i < ScheduleConfig.ALL_SCHEDULE_WEEK_NUMBER; ++i) {
  #   #     nurse.setSchedule(nurse.getSchedule() + nurse.getWeekPattern(i));
  #   #   }
  #   # }

  #   # return ScheduleUtil.changeAllDaysToProperShift(allNurses);


  # end

  # def all_nights_assigned?(patterns)
  #   patterns.transpose.all? { |day| day.count(:night?) == 1 }
  # end

  # def selector_for_resource(resource)
  #   shift_selectors[resource] ||= ShiftSelector.new(validator, resource)
  # end

  # def shift_selectors
  #   @shift_selectors ||= {}
  # end

  def validator
    @validator ||= ShiftValidator.new
  end

  def assigments
    plan.assigments
  end

  def days
    plan.dates
  end
end
