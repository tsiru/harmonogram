FactoryGirl.define do
  factory :resource do
    first_name     { Faker::Name.first_name }
    last_name      { Faker::Name.last_name }
    hours_per_week { {36 => 12,32 => 1,20 => 3}.map{ |hours,prop| Array.new(prop) { hours } }.flatten.sample }
  end

end
