module ControllerMacros
  module ExampleGroupMethods
    CRUD_ACTIONS = %i(new create update destroy index edit show)
    def expect_to_have_valid_controller_actions(only: CRUD_ACTIONS, except: [])
      actions = [*only] - [*except]

      let(:valid_params)  { build(model_key).attributes.symbolize_keys.select{ |k,v| k != :id && !v.nil? } } # without id and empty values

      let(:edit_item_url) { send(:"edit_#{model_key}_url", item) }
      let(:items_url)     { send(:"#{models_key}_url") }

      let(:model_class)   { basename_part.singularize.constantize }
      let(:model_key)     { basename_part.singularize.underscore.to_sym }
      let(:models_key)    { basename_part.underscore.to_sym }

      let(:basename_part) { described_class.name.gsub(/Controller$/, '') }

      describe '#new' do
        before { get :new }

        it { expect(response).to be_success }
        it { expect(assigns[model_key]).to be_a model_class }
        it { expect(assigns[model_key]).to be_new_record }
      end if actions.include? :new

      describe '#create' do
        before { post :create, model_key => valid_params }

        let(:item) { model_class.last }

        it { expect(response).to redirect_to edit_item_url }
        it { expect(item).to have_attributes(valid_params) }
        it { expect(flash[:notice]).to eql "#{model_class.model_name.human} was successfully created." }
      end if actions.include? :create

      context 'when some record exists' do
        let!(:item) { create model_key }

        describe '#show' do
          before { get :show, id: item.id }
          it { expect(response).to be_success }
        end if actions.include? :show

        describe '#index' do
          before { get :index }

          it { expect(response).to be_success }
          it { expect(assigns[models_key]).to be_include item }
        end if actions.include? :index

        describe '#destroy' do
          before { delete :destroy, id: item.id }
          it { expect(response).to redirect_to items_url }
          it { expect(item).to_not exists_in_db }
          it { expect(flash[:notice]).to eql "#{model_class.model_name.human} was successfully destroyed." }
        end if actions.include? :destroy

        describe '#edit' do
          before { get :edit, id: item.id, model_key => valid_params }
          it { expect(response).to be_success }
          it { expect(assigns[model_key]).to eql item }
        end if actions.include? :edit

        describe '#update' do
          before { patch :update, id: item.id, model_key => valid_params }
          it { expect(response).to redirect_to edit_item_url }
          it { expect(item.reload).to have_attributes valid_params }
          it { expect(flash[:notice]).to eql "#{model_class.model_name.human} was successfully updated." }
        end if actions.include? :update
      end
    end
  end

  def self.included(receiver)
    receiver.extend ExampleGroupMethods
    # receiver.send :include, ExampleMethods
  end
end
