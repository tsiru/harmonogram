require 'rspec/expectations'

RSpec::Matchers.define :exists_in_db do
  match do |record|
    begin
      record.class.find(record.id)
    rescue ActiveRecord::RecordNotFound
      false
    end
  end
end

RSpec::Matchers.define :have_attributes do |attributes_list|
  match do |record|
    attributes_list.all? do |key, value|
      a = record.send(key)
      puts "#{a.inspect} != #{value.inspect}" if a != value
      a == value
    end
  end

  failure_message do |record|
    diff_list = {}
    attributes_list.each { |k,v| diff_list[k] = record.send(k) if record.send(k) != v }
    "expected that #{record} contain folowing attributes: #{attributes_list.inspect}, but have: #{diff_list.inspect}"
  end

  failure_message_when_negated do |record|
    diff_list = {}
    attributes_list.each { |k,v| diff_list[k] = record.send(k) if record.send(k) == v }
    "expected that #{record} not contain folowing attributes: #{attributes_list.inspect}, but have: #{diff_list.inspect}"
  end
end
