require 'rails_helper'

RSpec.describe PlanGenerator::Pattern, type: :service do
  it { expect(pattern('RNR').or pattern('NRR')).to be_eql pattern('NNR') }
  it { expect(pattern('RNRR').rotate(-1)).to be_eql pattern('RRNR') }
  it { expect(pattern('RNR')).to be_eql pattern('RNR') }

  def pattern(*patterns)
    PlanGenerator::Pattern.new(patterns.join.chars.reject(&:blank?).map{ |char| Shift.find char } )
  end
end
