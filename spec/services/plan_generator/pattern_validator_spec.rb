require 'rails_helper'

RSpec.describe PlanGenerator::ShiftValidator, type: :service do
  subject { described_class.new }
  let(:fulltime)   { double(:fulltime,   inspect: 'fulltime worker',   max_shifts: 5,  allowed_shifts_type: Shift::REAL_TYPES ) }
  let(:halftime)   { double(:halftime,   inspect: 'halftime worker',   max_shifts: 2,  allowed_shifts_type: Shift::REAL_TYPES ) }
  let(:workoholic) { double(:workoholic, inspect: 'workoholic worker', max_shifts: 99, allowed_shifts_type: Shift::REAL_TYPES ) }

  describe 'For each day a nurse may start only one shift. ' # WAD
  describe 'A night shift has to be followed by at least 14 hours rest. An exception is that once in a period of 21 days for 24 consecutive hours, the resting time may be reduced to 8 hours.' # IGNORE IT

  describe 'Cover needs to be fulfilled' do
    pending
  end

  describe 'A nurse must receive at least 2 weekends off duty per 5 week period. A weekend off duty lasts 60 hours including Saturday 00:00 to Monday 04:00.' do
    it { is_expected.to_not be_valid_for_period pattern('AAARRAA','AAARRAA','AAARRAA''AAARRAA','AAARRAA'), workoholic }
    it { is_expected.to_not be_valid_for_period pattern('AAARRAA','AAARRAA','AAARRAA''AAAAARR','AAARRAA'), workoholic }
    it { is_expected.to     be_valid_for_period pattern('AAARRAA','AAAAARR','AAARRAA''AAAAARR','AAARRAA'), workoholic }
    it { is_expected.to     be_valid_for_period pattern('AAAAARR','AAAAARR','AAARRAA''AAARRAA','AAARRAA'), workoholic }
    it { is_expected.to     be_valid_for_period pattern('AAAAARR','AAAAARR','AAAAARR''AAAAARR','AAAAARR'), workoholic }
  end

  describe 'During any period of 24 consecutive hours, at least 11 hours of rest is required.' do
    pending
  end

  describe 'Within a scheduling period a nurse is allowed to exceed the number of hours for which they are available for their department by at most 4 hours.' do
    it { is_expected.to_not be_valid_for_week pattern('AAAAAAR'), fulltime }
    it { is_expected.to     be_valid_for_week pattern('AAAAARR'), fulltime }
    it { is_expected.to_not be_valid_for_week pattern('AAAAARR'), halftime }
    it { is_expected.to     be_valid_for_week pattern('ARRRARR'), halftime }
  end

  describe 'The maximum number of night shifts is 3 per period of 5 consecutive weeks.' do
    it { is_expected.to     be_valid_for_period pattern('AAAAARR','AAAAARR','AAAAARR''AAANNRR','AAAAARR'), workoholic }
    it { is_expected.to_not be_valid_for_period pattern('AAAAARR','AAAAARR','AAAAARR''AAANNRR','AAANNRR'), workoholic }
  end

  describe 'Following a series of at least 2 consecutive night shifts a 42 hours rest is required.' do
    it { is_expected.to_not be_valid_for_week pattern('NNARRRR'), workoholic }
    it { is_expected.to_not be_valid_for_week pattern('NNRARRR'), workoholic }
    it { is_expected.to     be_valid_for_week pattern('NNRRARR'), workoholic }
    it { is_expected.to_not be_valid_for_period pattern('RRARRNN', 'NRARAAA', 'AAAAARR', 'AAAAARR', 'AAAAARR'), workoholic }
    it { is_expected.to_not be_valid_for_period pattern('RRARRNN', 'RARRAAA', 'AAAAARR', 'AAAAARR', 'AAAAARR'), workoholic }
  end

  describe 'The number of consecutive night shifts is at most 3.' do
    it { is_expected.to     be_valid_for_week pattern('NNNRRRR'), workoholic }
    it { is_expected.to_not be_valid_for_week pattern('RNNNNRR'), workoholic }
    it { is_expected.to_not be_valid_for_week pattern('RRRNNNN'), workoholic }
    it { is_expected.to_not be_valid_for_period pattern('RRARRNN', 'NNRRRAA', 'AAAAARR', 'AAAAARR', 'AAAAARR'), workoholic }
    it { is_expected.to     be_valid_for_period pattern('RRARRRN', 'NNRRRAA', 'AAAAARR', 'AAAAARR', 'AAAAARR'), workoholic }
  end

  describe 'The number of consecutive shifts (workdays) is at most 6. ' do
    it { is_expected.to_not be_valid_for_week pattern('AAAAAAA'), workoholic }
  end

  describe '#have_conflicting_work_day?' do
    it { expect(pattern('RRRRRRR')).to_not be_have_conflicting_work_day pattern('RRRRRRR') }
    it { expect(pattern('NRRRRRR')).to_not be_have_conflicting_work_day pattern('RRRRRRR') }
    it { expect(pattern('NRRRRRR')).to_not be_have_conflicting_work_day pattern('RRRRRRR RRRRRRN') }
    it { expect(pattern('NRRRRRR RRRRRRN')).to_not be_have_conflicting_work_day pattern('RRRRRRR') }
    it { expect(pattern('NRRRRRR')).to     be_have_conflicting_work_day pattern('NRRRRRR') }
  end

  private

  def pattern(*patterns)
    PlanGenerator::Pattern.new(patterns.join.chars.reject(&:blank?).map{ |char| Shift.find char } )
  end
end
