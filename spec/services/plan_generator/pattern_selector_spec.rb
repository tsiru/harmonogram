require 'rails_helper'

RSpec.describe PlanGenerator::ShiftSelector, type: :service do
  let(:selector)  { described_class.new(validator, resource) }
  let(:resource)  { double(:resource, inspect: 'workoholic worker', max_shifts: 99, allowed_shifts_type: Shift::REAL_TYPES ) }
  let(:validator) { PlanGenerator::ShiftValidator.new }

  subject { selector }

  describe '#where' do
    let(:types)    { Shift::NIGHT }
    let(:matching) { [pattern('NRRRRRN')] }

    subject { selector.where(types: types, matching: matching) }

    context 'when types is Shift::NIGHT' do
      it { is_expected.to be_none { |pattern| pattern.first.work? } }
      it { is_expected.to be_all  { |pattern| pattern.first.free? } }
      it { is_expected.to be_none { |pattern| pattern.last.work? } }
      it { is_expected.to be_all  { |pattern| pattern.last.free? } }
    end
  end

  private

  def pattern(*patterns)
    PlanGenerator::Pattern.new(patterns.join.chars.reject(&:blank?).map{ |char| Shift.find char } )
  end
end
