require 'faker'

print 'Resource '
11.times do
  Resource.create! first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, hours_per_week: 36, allowed_shifts_type: Shift::REAL_TYPES
  print '*'
end
1.times do
  Resource.create! first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, hours_per_week: 36, allowed_shifts_type: (Shift::REAL_TYPES - [Shift::LATE])
  print '*'
end
3.times do
  Resource.create! first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, hours_per_week: 20, allowed_shifts_type: Shift::REAL_TYPES
  print '*'
end
1.times do
  Resource.create! first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, hours_per_week: 32, allowed_shifts_type: Shift::REAL_TYPES
  print '*'
end

puts " (#{Resource.count})"

print 'Plan '
1.times do
  Plan.create!(start_date: Date.today - (Date.today.wday-1).days).generate!
  print '*'
end

puts " (#{Plan.count})"
