class DropPlanEndDate < ActiveRecord::Migration
  def change
    remove_column :plans, :end_date
  end
end
