class ReorganizePlan < ActiveRecord::Migration
  def change
    add_column :assigments, :plan_id, :integer, unsigned: true
    remove_column :shifts, :plan_id
    drop_table :shifts
    add_column :assigments, :pattern, :string
  end
end
