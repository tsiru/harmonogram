class AddHoursToResources < ActiveRecord::Migration
  def change
    add_column :resources, :hours_per_week, :integer
  end
end
