class AddAllowedShiftTypesToResources < ActiveRecord::Migration
  def change
    add_column :resources, :allowed_shifts_type, :string
  end
end
