class CreateAssigments < ActiveRecord::Migration
  def change
    create_table :assigments do |t|
      t.references :resource, index: true, foreign_key: true
      t.references :shift, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
