class CreateResources < ActiveRecord::Migration
  def change
    create_table :resources do |t|
      t.string :first_name
      t.string :last_name
      t.references :position, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
