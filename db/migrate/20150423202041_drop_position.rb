class DropPosition < ActiveRecord::Migration
  def change
    drop_table :positions
    remove_column :resources, :position_id
  end
end
