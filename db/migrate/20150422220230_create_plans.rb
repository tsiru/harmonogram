class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.date :start_date
      t.date :end_date
      t.string :title

      t.timestamps null: false
    end
    add_index :plans, :start_date
    add_index :plans, :end_date
  end
end
