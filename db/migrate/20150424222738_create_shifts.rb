class CreateShifts < ActiveRecord::Migration
  def change
    create_table :shifts do |t|
      t.references :plan, index: true, foreign_key: true
      t.date :date
      t.string :shift_type

      t.timestamps null: false
    end
  end
end
