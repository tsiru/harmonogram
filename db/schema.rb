# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150513224337) do

  create_table "assigments", force: :cascade do |t|
    t.integer  "resource_id", limit: 4
    t.integer  "shift_id",    limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "pattern",     limit: 255
    t.integer  "plan_id",     limit: 4
  end

  add_index "assigments", ["resource_id"], name: "index_assigments_on_resource_id", using: :btree
  add_index "assigments", ["shift_id"], name: "index_assigments_on_shift_id", using: :btree

  create_table "plans", force: :cascade do |t|
    t.date     "start_date"
    t.string   "title",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "plans", ["start_date"], name: "index_plans_on_start_date", using: :btree

  create_table "resources", force: :cascade do |t|
    t.string   "first_name",          limit: 255
    t.string   "last_name",           limit: 255
    t.integer  "position_id",         limit: 4
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "hours_per_week",      limit: 4
    t.string   "allowed_shifts_type", limit: 255
  end

  add_index "resources", ["position_id"], name: "index_resources_on_position_id", using: :btree

end
