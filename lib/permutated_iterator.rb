days = [1,2,3,4]
resources = %w(A B C D)
shifts = %w(% #)

class PlanPermutator < Struct.new(:days, :shifts, :resources)

  def generate
    rrrrrrrr(days)
  end

  private

  def rrrrrrrr(days, prev_days = [], solution = {}, &block)
    days = days.clone
    prev_days = prev_days.clone
    prev_days << days.shift
    resources.permutation(shifts.count) do |day_resources|
      s = solution.clone

      if prev_days.count > 1 # at least one day before
        next if day_resources.any? { |res| solution.values.last.include? res } # was working in prev day
      end

      s[prev_days.last] = day_resources
      if days.any?
        rrrrrrrr(days, prev_days, s, &block)
      else
        block.call s
      end
    end
  end

  # rrrrrrrr(days, shifts, resources) do |solution|
  #   puts solution
  # end

end


__END__

days = [1,2,3,4]
resources = %w(A B C D)
shifts = %w(% #)

def generate(days, shifts, resources, prev_days = [], solution = {}, &block)
  days = days.clone
  prev_days = prev_days.clone
  prev_days << days.shift
  resources.permutation(shifts.count) do |day_resources|
    s = solution.clone

    if prev_days.count > 1 # at least one day before
      next if day_resources.any? { |res| solution.values.last.include? res } # was working in prev day
    end

    s[prev_days.last] = day_resources
    if days.any?
      generate(days, shifts, resources, prev_days, s, &block)
    else
      block.call s
    end
  end
end

generate(days, shifts, resources) do |solution|
  puts solution
end
